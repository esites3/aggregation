library(tidyverse)
library(readr)

df_needs <- read.csv("C:/Users/ElsbethSites/Box/Private Files - Sites, Elsbeth/Health equity data request/healthequitydata.csv", na.strings=c("","NA")) %>%
  # clean variable names
  setNames(gsub("...",".", names(.), fixed = TRUE)) %>%
  setNames(gsub("..",".", names(.), fixed = TRUE)) %>%
  rename('hl.client.id.phi' = 'HL.Client.ID.Protected.Health.Information.') %>%
  setNames(tolower(names(.))) 

# make a dataframe of unique HL client IDs (rather than unique needs)
 df_patient <- df_needs %>%
   group_by(hl.client.id.phi) %>%
   select(hl.client.id.phi, case.desk.location, race, ethnicity, insurance.status, preferred.language)

Dems_fun <- function(data, race, ethnicity, lang, insurance) {
  # enquo all these variables
  race_var <- enquo(race)
  eth_var <- enquo(ethnicity)
  lang_var <- enquo(lang)
  ins_var <- enquo(insurance)
  
  
  Fun <- function(data, ...) {
    group_var <- quos(...)
    
    data %>% 
      group_by(!!! group_var) %>%
      summarise (n = n()) %>%
      mutate(freq = n / sum(n)) %>%
      unite(dem, !!! group_var, sep = "_", remove = T)
  }
  
  # unquote all these variables
  race <- Fun(data, !!race_var)
  ethnicity <- Fun(data, !!eth_var)
  preferred.language <- Fun(data, !!lang_var)
  insurance.status <- Fun(data, !!ins_var)
  
  
  Dems <- rbind(race, ethnicity)
  colnames(Dems) <- c("Category", "count", "percentage")
  return(Dems)
}

df2 <- Dems_fun(df_patient, race, ethnicity, preferred.language, insurance.status)


# 
# 
# demo_count <- function(d, v){
#   d %>%
#     group_by(v) %>%
#     summarize(n_patients = n())
# }
# 
# 
# demo_count(df_patient, race)
# 
# #df_patient %>%
# #  summarize_at(c("race", "ethnicity", "insurance.status", "preferred.language"), n_distinct)
# 
# df_patient %>%
#   group_by(insurance.status) %>%
#   summarize(n_patients = n())
# 
# 
# 
# 
# 


